from django.apps import AppConfig, apps

from rest_auth import models

class CoreAppConfig(AppConfig):

    name = 'core'

    def ready(self):
        models.setup_users()
        try:
            self.build_data()
        except:
            pass

    def build_data(self):
        from django.contrib.auth.models import User
        root, created = User.objects.get_or_create(defaults=dict(
            is_superuser=True, is_staff=True, is_active=True,
        ), username='root')
        if created:
            root.set_password('password')
            root.save()
        #
        User.objects.get_or_create(defaults=dict(
            is_active=True, email='alice@z.io', first_name='Alice', last_name='Adam',
        ), username='alice')
        User.objects.get_or_create(defaults=dict(
            is_active=True, email='bob@z.io', first_name='Bob', last_name='Blue',
        ), username='bob')
        User.objects.get_or_create(defaults=dict(
            is_active=True, email='cindy@z.io', first_name='Cindy', last_name='Cola',
        ), username='cindy')
        #
