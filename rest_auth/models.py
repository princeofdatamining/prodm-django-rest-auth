from django.db import models
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from django.conf import settings
from django.apps import apps

from . import constant

USER = settings.AUTH_USER_MODEL
def setup_users(): import_user_model(globals(), USER)

def import_user_model(vars, USER_MODEL=None):
    if 'Users' in vars:
        return 
    model = apps.get_model(USER_MODEL or settings.AUTH_USER_MODEL)
    vars['User'], vars['Users'] = model, model.objects
