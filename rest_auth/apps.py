from django.apps import AppConfig

from . import constant

class QAppConfig(AppConfig):

    name = constant.APP_LABEL
    verbose_name = constant.APP_VERBOSE_NAME

    def ready(self):
        from .models import setup_users
        setup_users()
