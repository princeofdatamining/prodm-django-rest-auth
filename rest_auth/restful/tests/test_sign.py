# from rest_framework.test import APITestCase
from .base import APITestCase
from rest_framework.settings import api_settings
from rest_framework import status
from rest_framework.reverse import reverse

from django.conf import settings
from qc_utils.django import i18n

from rest_auth.restful.utils import obtain_auth_token

class TestSign(APITestCase):

    LANGUAGE = 'en'

    def setUp(self):
        super(TestSign, self).setUp()
        #
        self.user_01 = (u01, p01) = ('user01', 'Pa**word')
        self.user_10 = (u10, p10) = ('user10', 'pa**Word')
        self.force_user(u01, password=p01)
        self.force_user(u10, password=p10, is_active=False)
        #
        self.signed_url = self.reverse_api('signed-or-not')
        self.signin_url = self.reverse_api('sign-in')
        self.signout_url = self.reverse_api('sign-out')

    # 测试｀登入｀的 Bad Request（400）
    def test_signin_fail(self):
        self.check_signin_nothing()
        self.check_signin_null()
        self.check_signin_blank()
        self.check_signin_notexists()
        self.check_signin_password()
        self.check_signin_inactive()

    def check_signin_nothing(self):
        resp = self.client.post(self.signin_url, {
        })
        self.assertEqual(400, resp.status_code)
        # self.assertEqual({}, resp.data)
        self.assertEqual(2, len(resp.data))
        self.assert400reason(i18n.E_REQUIRED_VALUE, resp.data['username'])
        self.assert400reason(i18n.E_REQUIRED_VALUE, resp.data['password'])

    def check_signin_null(self):
        resp = self.client.post(self.signin_url, {
            'username': None,
            'password': None,
        }, format='json')
        self.assertEqual(400, resp.status_code)
        # self.assertEqual({}, resp.data)
        self.assertEqual(2, len(resp.data))
        self.assert400reason((i18n.E_NULL_VALUE, i18n.E_NULL_VALUE2), resp.data['username'])
        self.assert400reason((i18n.E_NULL_VALUE, i18n.E_NULL_VALUE2), resp.data['password'])

    def check_signin_blank(self):
        resp = self.client.post(self.signin_url, {
            'username': '',
            'password': '',
        })
        self.assertEqual(400, resp.status_code)
        # self.assertEqual({}, resp.data)
        self.assertEqual(2, len(resp.data))
        self.assert400reason((i18n.E_BLANK_VALUE, i18n.E_BLANK_VALUE2), resp.data['username'])
        self.assert400reason((i18n.E_BLANK_VALUE, i18n.E_BLANK_VALUE2), resp.data['password'])

    def check_signin_notexists(self):
        resp = self.client.post(self.signin_url, {
            'username': 'blahblah',
            'password': 'blahblah',
        })
        self.assertEqual(400, resp.status_code)
        # self.assertEqual({}, resp.data)
        self.assertEqual(1, len(resp.data))
        self.assert400reason(i18n.E_USER_NOT_EXISTS, resp.data['username'])

    def check_signin_password(self):
        resp = self.client.post(self.signin_url, {
            'username': self.user_01[0],
            'password': 'blahblah',
        })
        self.assertEqual(400, resp.status_code)
        # self.assertEqual({}, resp.data)
        self.assertEqual(1, len(resp.data))
        self.assert400reason(i18n.E_PASSWORD_INCORRECT, resp.data['password'])

    def check_signin_inactive(self):
        resp = self.client.post(self.signin_url, {
            'username': self.user_10[0],
            'password': self.user_10[1],
        })
        self.assertEqual(400, resp.status_code)
        # self.assertEqual({}, resp.data)
        self.assertEqual(1, len(resp.data))
        self.assert400reason(i18n.E_USER_INACTIVE, resp.data['username'])

    # 测试｀登入｀及｀登出｀及｀是否登入｀
    def test_in_and_out(self):
        self.check_not_signed() # 初始未登入
        self.check_signin() # 登入
        self.check_signed() # 已登入
        self.check_signout() # 登入
        self.check_not_signed() # 未登入
        self.check_token_signed() # 用token来保证已登入

    def check_not_signed(self):
        resp = self.client.get(self.signed_url)
        self.assertEqual(404, resp.status_code)
        # self.assertEqual({}, resp.data)

    def check_signed(self):
        resp = self.client.get(self.signed_url)
        self.assertEqual(200, resp.status_code)
        self.assertEqual(self.userid, resp.data['id'])

    def check_token_signed(self):
        resp = self.client.get(self.signed_url, HTTP_AUTHORIZATION='token '+self.token)
        self.assertEqual(200, resp.status_code)
        self.assertEqual(self.userid, resp.data['id'])

    def check_signin(self):
        resp = self.client.post(self.signin_url, {
            'username': self.user_01[0],
            'password': self.user_01[1],
        })
        self.assertEqual(200, resp.status_code)
        self.assertIn('token', resp.data)
        self.token = resp.data['token']
        self.assertTrue(bool(self.token))
        self.userid = resp.data['id']

    def check_signout(self):
        resp = self.client.get(self.signout_url)
        self.assertEqual(200, resp.status_code)
