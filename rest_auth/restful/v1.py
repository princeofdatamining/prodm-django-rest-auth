from rest_framework import validators, permissions, status
from rest_framework.generics import *
from rest_framework.permissions import *
from rest_framework.serializers import *
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response

from django.conf import settings
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.password_validation import validate_password

from qc_utils.rest_framework.views import *
from qc_utils.rest_framework.permissions import *
from qc_utils.rest_framework.serializers import *
from qc_utils.rest_framework.fields import *
from qc_utils.django import i18n

from .. import models, constant
from . import utils

private_fields = profile_fields = ('email', 'first_name', 'last_name')
public_fields = ('id', 'username', 'stringify')

class UserProfileSerializer(ModelSerializer):

    class Meta:
        model = models.User
        fields = profile_fields

class UserSerializer(ModelSerializer):

    class Meta:
        model = models.User
        fields = public_fields + private_fields
UserPublic = build_serializer(UserSerializer, *public_fields)

#

class SignInSerializer(Serializer):

    default_error_messages = {
        'user_not_exists': i18n.E_USER_NOT_EXISTS,
        'user_inactive': i18n.E_USER_INACTIVE,
        'pwd_incorrect': i18n.E_PASSWORD_INCORRECT,
        'captcha': constant.E_CAPTCHA,
    }

    username = CharField(label=i18n.USERNAME)
    password = CharField(label=i18n.PASSWORD)

    CAPTCHA = getattr(settings, 'CAPTCHA_VIEWS', {}).get('sign-in')

    def validate_username(self, value):
        if not value:
            return value
        self.instance = authenticate(username=value, password=False, check_active=False)
        if not self.instance:
            self.fail('user_not_exists')
        if not self.instance.is_active:
            self.fail('user_inactive')
        return value

    def validate_password(self, value):
        if value and self.instance and not self.instance.check_password(value):
            self.fail('pwd_incorrect')
        return value

class SignInView(GenericAPIView):

    queryset = models.Users.none()
    submit_serializer_class = SignInSerializer
    serializer_class = UserPublic

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.instance
        if not user:
            raise Http404
        login(request, user)
        #
        self.change_serializer_data(serializer)
        serializer._data['token'] = token = utils.obtain_auth_token(user)
        return Response(serializer.data)

class SignOutView(RetrieveAPIView):

    def get(self, request):
        logout(request)
        return Response({})

class SignedView(RetrieveAPIView):

    serializer_class = UserPublic

    def get(self, request):
        if not request.user.pk:
            raise Http404
        serializer = self.get_serializer(instance=request.user)
        return Response(serializer.data)

#

class ChangePasswordSerializer(Serializer):

    default_error_messages = {
        'incorrectly': constant.E_INCORRECT_OLD_PASSWORD,
    }

    old_password = CharField(label=constant.OLD_PASSWORD, write_only=True)
    new_password = CharField(label=constant.NEW_PASSWORD, write_only=True)

    def validate_old_password(self, value):
        user = self.context['request'].user
        if not user.check_password(value):
            self.fail('incorrectly')
        return value

    def validate_new_password(self, value):
        validate_password(value)
        return value

class ChangePasswordView(UpdateOnlyAPIView):

    permission_classes = (IsAuthenticated,)
    queryset = models.Users.none()
    serializer_class = Serializer
    submit_serializer_class = ChangePasswordSerializer

    def get_object(self):
        return self.request.user

    def perform_update(self, serializer):
        user, password = serializer.instance, serializer.validated_data['new_password']
        user.set_password(password)
        user.save()
        #
        user = authenticate(username=user.username, password=password)
        login(self.request, user)
        #
        serializer._data = {}

#

class ProfileView(RetrievePatchAPIView):

    queryset = models.Users.filter(is_superuser=False, is_staff=False)
    serializer_class = UserPublic
    # 已登陆用户查看自己，会有额外的信息
    owner_fields = 'pk'
    owner_serializer_class = build_serializer(UserSerializer, *public_fields, *private_fields)
    # 已登陆方可修改自己的资料
    permission_classes = (make_owner_permission('pk'),)
    submit_serializer_class = UserProfileSerializer

class UsersView(ListAPIView):

    serializer_class = UserPublic
    queryset = models.Users.filter(is_superuser=False, is_staff=False)

#

from django.conf.urls import url, include
urlpatterns = [
    url(r'^signin/$', SignInView.as_view(), name='sign-in'),
    url(r'^signout/$', SignOutView.as_view(), name='sign-out'),
    #
    url(r'^signed/$', SignedView.as_view(), name='signed-or-not'),
    url(r'^password/change$', ChangePasswordView.as_view(), name='password-change'),
    #
    url(r'^users/$', UsersView.as_view(), name='user-info-list'),
    url(r'^users/(?P<pk>\d+|~)$', ProfileView.as_view(), name='info-or-profile'),
    #
]
from rest_framework.urlpatterns import format_suffix_patterns
urlpatterns = format_suffix_patterns(urlpatterns)
