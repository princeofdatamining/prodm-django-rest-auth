from django.contrib.auth.backends import ModelBackend
from django.contrib.auth import get_user_model

class ModelBackend(ModelBackend):

    def authenticate(self, username=None, password=None, **kwargs):
        check_active = kwargs.pop('check_active', True)
        #
        UserModel = get_user_model()
        if username is None:
            username = kwargs.get(UserModel.USERNAME_FIELD)
        try:
            user = UserModel._default_manager.get_by_natural_key(username)
            if self.check_password(user, password):
                return user
        except UserModel.DoesNotExist:
            # Run the default password hasher once to reduce the timing
            # difference between an existing and a non-existing user (#20760).
            UserModel().set_password(password)

    def check_password(self, user, password):
        return password is False or (password and user.check_password(password))
