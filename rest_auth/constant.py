from django.utils.translation import ugettext_lazy as _, ungettext_lazy, pgettext_lazy, npgettext_lazy

APP_LABEL = 'rest_auth'
APP_VERBOSE_NAME = 'rest_auth'

OLD_PASSWORD = _('Old password')
NEW_PASSWORD = _('New password')
E_INCORRECT_OLD_PASSWORD = _("Your old password was entered incorrectly. Please enter it again.")

E_CAPTCHA = _("Invalid CAPTCHA")
