# Django Restful Authentication

Current version: 1.0.1

django-rest-auth is a Django & rest_framework application:

* 实现了 登入、登出、是否登入、修改密码 等常用功能的 RESTFUL 接口;
* 根据 settings.AUTH_USER_MODEL 智能获取User Model，跟实际的用户模型解藕;
